package main;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    private final int FIELD_SIZE = 30;
    private Board board;
    private Button[][] buttonArray;
    private boolean isFirstMove;
    private int numberOfFlags;
    private boolean gameIsOver;
    private boolean gameIsWon;

    @FXML
    private AnchorPane pane;
    @FXML
    private GridPane grid;
    @FXML
    private Label NumberOfFlagsLabel;
    @FXML
    private ChoiceBox difficultyChoiceBox;
    @FXML
    private Button startButton;

    @FXML
    private void startOnAction(ActionEvent event) {
        remakeTheBoard();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        difficultyChoiceBox.getItems().addAll("Easy", "Regular", "Hard", "Extreme");
        difficultyChoiceBox.getSelectionModel().select(1);
        makeGameBoard(20, 20, 75, 2);
    }

    // making the game board
    private void makeGameBoard(int xHEIGHT, int xWIDTH, int xNUMBER_OF_MINES, int xSIZE_OF_FIRST_SQUARE) {
        numberOfFlags = 0;
        gameIsOver = false;
        gameIsWon = false;
        isFirstMove = true;
        board = new Board(xHEIGHT, xWIDTH, xNUMBER_OF_MINES, xSIZE_OF_FIRST_SQUARE);
        NumberOfFlagsLabel.setText((board.getNUMBER_OF_MINES() - numberOfFlags) + "");
        try {
            board.setFields();
        } catch (EndlessLoopException e) {
            e.printStackTrace();
        }
        buttonArray = new Button[board.getHEIGHT()][board.getWIDTH()];
        pane.setMinSize(board.getHEIGHT() * FIELD_SIZE + 25, board.getWIDTH() * FIELD_SIZE + 60);
        grid.getChildren().clear();
        for (int h = 0; h < board.getHEIGHT(); h++) {
            for (int w = 0; w < board.getWIDTH(); w++) {
                final int H = h,
                        W = w;
                buttonArray[H][W] = new Button();

                buttonArray[H][W] = convertField(H, W, buttonArray[H][W]);
                cover(H, W);
                grid.add(buttonArray[H][W], H, W);

                buttonArray[H][W].setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        MouseButton button = event.getButton();
                        if (button == MouseButton.PRIMARY) {
                            if (isFirstMove) {
                                try {
                                    board.setFields(H, W);
                                } catch (EndlessLoopException e) {
                                    e.printStackTrace();
                                }
                                isFirstMove = false;
                                for (int h = 0; h < board.getHEIGHT(); h++)
                                    for (int w = 0; w < board.getWIDTH(); w++) {
                                        buttonArray[h][w].setText(board.getFieldArray()[h][w].getType() + "");
                                        buttonArray[h][w] = convertField(h, w, buttonArray[h][w]);
                                        if (!board.getFieldArray()[h][w].isHidden())
                                            board.checkTheField(h, w);
                                    }
                                board.checkTheField(H, W);
                            } else
                                board.checkTheField(H, W);
                            checkIfVictory();
                            uncoverAllVisableFields();
                            checkIfOver();
                        } else if (button == MouseButton.SECONDARY) {
                            if (board.getFieldArray()[H][W].isFlagged() && board.getFieldArray()[H][W].isHidden())
                                unflag(H, W);
                            else if (board.getFieldArray()[H][W].isHidden())
                                flag(H, W);
                        }
                    }
                });
            }

        }
    }

    // remaking the board - change difficult options here
    public void remakeTheBoard() {
        Stage primaryStage = (Stage) startButton.getScene().getWindow();
        int height, width, numberOfMines, sizeOfFirstSquare;
        if (difficultyChoiceBox.getValue().toString().compareTo("Easy") == 0) {
            height = 16;
            width = 16;
            numberOfMines = 35;
            sizeOfFirstSquare = 1;
        } else if (difficultyChoiceBox.getValue().toString().compareTo("Regular") == 0) {
            height = 20;
            width = 20;
            numberOfMines = 75;
            sizeOfFirstSquare = 2;
        } else if (difficultyChoiceBox.getValue().toString().compareTo("Hard") == 0) {
            height = 30;
            width = 20;
            numberOfMines = 180;
            sizeOfFirstSquare = 2;
        } else {
            height = 35;
            width = 25;
            numberOfMines = 280;
            sizeOfFirstSquare = 2;
        }
        changeSizeOfStage(height * FIELD_SIZE + 41, width * FIELD_SIZE + 99, primaryStage);
        makeGameBoard(height, width, numberOfMines, sizeOfFirstSquare);
    }

    // "convert" the field to the button, sets all buttons parameters
    private Button convertField(final int H, final int W, Button button) {
        buttonArray[H][W].setMinSize(FIELD_SIZE, FIELD_SIZE);
        buttonArray[H][W].setFont(Font.font("Verdana", FontWeight.BOLD, 22));
        buttonArray[H][W].setPadding(new Insets(0));
        buttonArray[H][W].setBorder(new Border(new BorderStroke(Color.DARKGRAY,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        if (board.getFieldArray()[H][W].getType() == '0') {
            buttonArray[H][W].setText(" ");
        } else if (board.getFieldArray()[H][W].getType() == '1') {
            buttonArray[H][W].setTextFill(Color.BLUE);
        } else if (board.getFieldArray()[H][W].getType() == '2') {
            buttonArray[H][W].setTextFill(Color.GREEN);
        } else if (board.getFieldArray()[H][W].getType() == '3') {
            buttonArray[H][W].setTextFill(Color.RED);
        } else if (board.getFieldArray()[H][W].getType() == '4') {
            buttonArray[H][W].setTextFill(Color.DARKORANGE);
        } else if (board.getFieldArray()[H][W].getType() == '5') {
            buttonArray[H][W].setTextFill(Color.DARKRED);
        } else if (board.getFieldArray()[H][W].getType() == '6') {
            buttonArray[H][W].setTextFill(Color.DARKVIOLET);
        } else if (board.getFieldArray()[H][W].getType() == '7') {
            buttonArray[H][W].setTextFill(Color.DEEPPINK);
        } else if (board.getFieldArray()[H][W].getType() == '8') {
            buttonArray[H][W].setTextFill(Color.AZURE);
        }
        return button;
    }

    // uncover the regular field - if its mine sets mineImage
    private void uncover(final int H, final int W) {
        if (!gameIsOver) {
            Image mineImage = new Image("file:graphic/mine.png");
            if (board.getFieldArray()[H][W].getType() == 'M') {
                try {
                    buttonArray[H][W].setGraphic(new ImageView(mineImage));
                    board.getFieldArray()[H][W].unhide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // uncover all mines
                for (int h = 0; h < board.getHEIGHT(); h++) {
                    for (int w = 0; w < board.getWIDTH(); w++) {
                        if (board.getFieldArray()[h][w].getType() == 'M') {
                            buttonArray[h][w].setGraphic(new ImageView(mineImage));
                            board.getFieldArray()[h][w].unhide();
                        }
                    }
                }
                gameIsOver = true;
            } else {
                buttonArray[H][W].setGraphic(null);
                board.getFieldArray()[H][W].unhide();
            }
        }
    }

    private void flag(final int H, final int W) {
        if (!gameIsOver && !isFirstMove && (board.getNUMBER_OF_MINES() - numberOfFlags) > 0) {
            numberOfFlags++;
            Image flagImage = new Image("file:graphic/flag.png");
            if (board.getFieldArray()[H][W].isHidden() && !board.getFieldArray()[H][W].isFlagged()) {
                try {
                    buttonArray[H][W].setGraphic(new ImageView(flagImage));
                    board.getFieldArray()[H][W].flag();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            NumberOfFlagsLabel.setText((board.getNUMBER_OF_MINES() - numberOfFlags) + "");
        }
    }

    private void cover(final int H, final int W) {
        Image coverImage = new Image("file:graphic/cover.png");
        try {
            buttonArray[H][W].setGraphic(new ImageView(coverImage));
            board.getFieldArray()[H][W].unflag();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unflag(final int H, final int W) {
        if (!gameIsOver) {
            if (board.getFieldArray()[H][W].isFlagged())
                numberOfFlags--;
            cover(H, W);
        }
        NumberOfFlagsLabel.setText((board.getNUMBER_OF_MINES() - numberOfFlags) + "");
    }

    private void uncoverAllVisableFields() {
        for (int h = 0; h < board.getHEIGHT(); h++)
            for (int w = 0; w < board.getWIDTH(); w++)
                if (!board.getFieldArray()[h][w].isHidden())
                    uncover(h, w);
    }

    private void changeSizeOfStage(final int H, final int W, Stage xStage) {
        // convert height (row) to stage width and width (row) to stage height
        xStage.setMaxHeight(W);
        xStage.setMaxWidth(H);
        xStage.setMinHeight(W);
        xStage.setMinWidth(H);
    }

    private void checkIfVictory() {
        boolean tempFlag = true;
        for (int h = 0; h < board.getHEIGHT(); h++)
            for (int w = 0; w < board.getWIDTH(); w++)
                if (board.getFieldArray()[h][w].getType() != 'M' && board.getFieldArray()[h][w].isHidden()) {
                    tempFlag = false;
                    break;
                }
        gameIsWon = tempFlag;
        if (gameIsWon)
            gameIsOver = true;

    }

    private void checkIfOver() {
        if (gameIsOver) {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/MessageScreen.fxml"));
            Parent root = null;
            Stage messStage = new Stage();
            messStage.setTitle("Message window");
            try {
                root = loader.load();
                messStage.setScene(new Scene(root));
            } catch (IOException e) {
                e.printStackTrace();
            }
            messStage.show();
            MessageController messageController = loader.getController();
            messageController.setMainController(this);
            messageController.setMessageStage(messStage);
            if (gameIsWon)
                messageController.changeText();
        }
    }
}
