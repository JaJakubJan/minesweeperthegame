package main;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MessageController implements Initializable {
    private MainController mainController;
    private Stage messageStage;

    @FXML
    private Label communicateLabel;
    @FXML
    private Label congratsLabel;

    @FXML
    private void newGameOnAction(ActionEvent event) {
        mainController.remakeTheBoard();
        messageStage.close();
    }
    @FXML
    private void quitOnAction(ActionEvent event) {
        Platform.exit();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setMessageStage(Stage messageStage) {
        this.messageStage = messageStage;
    }

    public void changeText(){
        communicateLabel.setText("VICTORY");
        congratsLabel.setText("Congratulations!");
    }
}
