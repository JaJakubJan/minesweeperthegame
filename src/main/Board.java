package main;

public class Board {
    final private int
            SIZE_OF_FIRST_SQUARE,
            NUMBER_OF_MINES,
            HEIGHT,
            WIDTH;
    private Field[][] fieldArray;
    private int settedMines;

    public Board(int xHEIGHT, int xWIDTH, int xNUMBER_OF_MINES, int xSIZE_OF_FIRST_SQUARE) throws NegativeArraySizeException {
        HEIGHT = xHEIGHT;
        WIDTH = xWIDTH;
        // protection against setting incorrect number of mines
        NUMBER_OF_MINES = (xNUMBER_OF_MINES > (xHEIGHT * xWIDTH) / 2) ? (xHEIGHT * xWIDTH) / 2 : xNUMBER_OF_MINES;
        SIZE_OF_FIRST_SQUARE = xSIZE_OF_FIRST_SQUARE;
        fieldArray = new Field[HEIGHT][WIDTH];
        for (int i = 0; i < HEIGHT; i++)
            for (int j = 0; j < WIDTH; j++)
                fieldArray[i][j] = new Field();
    }

    public Board() {
        this(10, 10, 15, 1);
    }

    public class Field {
        private char type;
        // 'U' - unknown
        // '0' - blank
        // 'M' - mined
        // numbers '1' to '8'
        private boolean isHidden;
        private boolean isFlagged;

        private Field() {
            type = 'U';
            isHidden = true;
            isFlagged = false;
        }

        public void unhide() {
            isHidden = false;
        }

        public void hide() {
            isHidden = true;
        }

        public void flag() {
            isFlagged = true;
        }

        public void unflag() {
            isFlagged = false;
        }

        // getters and setters
        private void setType(char xType) {
            type = xType;
        }

        public char getType() {
            return type;
        }

        public boolean isHidden() {
            return isHidden;
        }

        public boolean isFlagged() {
            return isFlagged;
        }
    }

    // building the 'vanilla" version of our board - setting all fields
    public void setFields() throws EndlessLoopException {
        int securityCounter = 0;
        while (settedMines != NUMBER_OF_MINES) {
            setMines();
            securityCounter++;
            if (securityCounter > 100000)
                throw new EndlessLoopException();
        }
        for (int h = 0; h < HEIGHT; h++) {
            for (int w = 0; w < WIDTH; w++)
                if (fieldArray[h][w].getType() != 'M' && checkNeighborsVisible(h, w))
                    fieldArray[h][w].setType((char) ('0' + neighboringMines(h, w)));
        }
    }

    public void setFields(final int H, final int W) throws EndlessLoopException {
        int securityCounter = 0;
        for (int h = H - SIZE_OF_FIRST_SQUARE; h < H + 1 + SIZE_OF_FIRST_SQUARE; h++) {
            for (int w = W - SIZE_OF_FIRST_SQUARE; w < W + 1 + SIZE_OF_FIRST_SQUARE; w++) {
                if (h < 0 || w < 0 || h > HEIGHT - 1 || w > WIDTH - 1)
                    continue;
                fieldArray[h][w].unhide();
                if (fieldArray[h][w].getType() == 'M') {
                    fieldArray[h][w].setType('0');
                    settedMines--;
                }
            }
        }
        while (settedMines != NUMBER_OF_MINES) {
            setMines();
            securityCounter++;
            if (securityCounter > 1000)
                throw new EndlessLoopException();
        }
        for (int h = 0; h < HEIGHT; h++) {
            for (int w = 0; w < WIDTH; w++)
                if (fieldArray[h][w].getType() != 'M')
                    fieldArray[h][w].setType((char) ('0' + neighboringMines(h, w)));
        }
    }

    private void setMines() {
        int tempRandomInt;
        for (int h = 0; h < HEIGHT; h++) {
            if (settedMines == NUMBER_OF_MINES)
                break;
            for (int w = 0; w < WIDTH; w++) {
                tempRandomInt = (int) (Math.random() * HEIGHT * WIDTH);
                if (tempRandomInt == 0) {
                    if (fieldArray[h][w].isHidden && (fieldArray[h][w].getType() == 'U' ||
                            // version for second setting (after first move)
                            fieldArray[h][w].getType() == '0' && checkNeighborsVisible(h, w))) {
                        fieldArray[h][w].setType('M');
                        settedMines++;
                        if (settedMines == NUMBER_OF_MINES)
                            break;
                    }
                }
            }
        }
    }

    // after first move we have to reallocate some mines (version without first or last column/row)
    // checking the visible of the neighboring fields
    private boolean checkNeighborsVisible(final int H, final int W) {
        for (int h = H - 1; h < H + 2; h++)
            for (int w = W - 1; w < W + 2; w++) {
                if (h < 0 || w < 0 || h > HEIGHT - 1 || w > WIDTH - 1)
                    continue;
                if (!fieldArray[h][w].isHidden)
                    return false;
            }
        return true;
    }

    // return value of neighboring mines of this location (within itself)
    private int neighboringMines(final int H, final int W) {
        int numberOfNeighbors = 0;
        for (int h = H - 1; h < H + 2; h++)
            for (int w = W - 1; w < W + 2; w++) {
                if (h < 0 || w < 0 || h > HEIGHT - 1 || w > WIDTH - 1)
                    continue;
                if (fieldArray[h][w].getType() == 'M')
                    numberOfNeighbors++;
            }
        return numberOfNeighbors;
    }

    // check the field of this position, return false if game is over and recursive unhide all blank fields in the area
    public boolean checkTheField(final int H, final int W) {
        if (H < 0 || W < 0 || H > HEIGHT - 1 || W > WIDTH - 1)
            return false;
        fieldArray[H][W].unhide();
        if (fieldArray[H][W].getType() == 'M')
            return false;
        else if (fieldArray[H][W].getType() == '0') {
            for (int h = H - 1; h < H + 2; h++)
                for (int w = W - 1; w < W + 2; w++) {
                    if (h < 0 || w < 0 || h > HEIGHT - 1 || w > WIDTH - 1)
                        continue;
                    if (fieldArray[h][w].getType() != '0')
                        fieldArray[h][w].unhide();
                    else if (fieldArray[h][w].isHidden) {
                        fieldArray[h][w].unhide();
                        checkTheField(h, w);
                    }
                }
        }
        return true;
    }

    // print the whole board - FOR TESTS
    public void print() {
        for (int h = 0; h < HEIGHT; h++) {
            for (int w = 0; w < WIDTH; w++)
                System.out.print(fieldArray[h][w].getType() + "   ");
            System.out.println();
        }
    }

    // print the visible - FOR TESTS
    public void printVisible() {
        for (int h = 0; h < HEIGHT; h++) {
            for (int w = 0; w < WIDTH; w++)
                if (fieldArray[h][w].isHidden)
                    System.out.print('H' + "   ");
                else
                    System.out.print('V' + "   ");
            System.out.println();
        }
    }

    // getters and setters
    public int getSettedMines() {
        return settedMines;
    }

    public int getNUMBER_OF_MINES() {
        return NUMBER_OF_MINES;
    }

    public int getHEIGHT() {
        return HEIGHT;
    }

    public int getWIDTH() {
        return WIDTH;
    }

    public Field[][] getFieldArray() {
        return fieldArray;
    }

    public int getSIZE_OF_FIRST_SQUARE() {
        return SIZE_OF_FIRST_SQUARE;
    }
}
