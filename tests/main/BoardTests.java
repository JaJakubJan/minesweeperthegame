package main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


class BoardTests {
    private final int FIRST_SHOT_H = 4;
    private final int FIRST_SHOT_W = 4;

    @Test
    void checkReallocatingMines() {
        boolean areMinesRejected = true;
        boolean isEveryMinesSetted = true;
        Board b = new Board();
        try {
            b.setFields();
            b.setFields(FIRST_SHOT_H , FIRST_SHOT_W);
        } catch (EndlessLoopException e) {
            e.printStackTrace();
        }
        for (int h = b.getHEIGHT() - b.getSIZE_OF_FIRST_SQUARE(); h < b.getHEIGHT() + 1 + b.getSIZE_OF_FIRST_SQUARE(); h++)
            for (int w = b.getWIDTH() - b.getSIZE_OF_FIRST_SQUARE(); w < b.getWIDTH() + 1 + b.getSIZE_OF_FIRST_SQUARE(); w++) {
                if (h < 0 || w < 0 || h > b.getHEIGHT() - 1 || w > b.getWIDTH() - 1)
                    continue;
                if (b.getFieldArray()[h][w].getType() == 'M') {
                    areMinesRejected = false;
                    break;
                }
            }
        if(b.getNUMBER_OF_MINES() != b.getSettedMines())
            isEveryMinesSetted = false;
        assertTrue(areMinesRejected, "WRONG MINES REJECTING");
        assertTrue(isEveryMinesSetted, "NO ALL MINES ARE SETTED");
    }

    // Testing board building with first shot
    @Test
    void printTestField() {
        Board b = new Board();
        System.out.println("____________PRINTING TEST_____________");
        try {
            b.setFields();
            b.setFields(FIRST_SHOT_H , FIRST_SHOT_W);
            b.print();
            System.out.println("____________PRINTING VISION___________");
            b.printVisible();
            for (int h = 0; h < b.getHEIGHT(); h++)
                for (int w = 0; w < b.getWIDTH(); w++) {
                    if (!b.getFieldArray()[h][w].isHidden())
                        b.checkTheField(h, w);
                }
            System.out.println("__________PRINTING REAL VISION________");
            b.printVisible();
        } catch (EndlessLoopException e) {
            e.printStackTrace();
        }
        System.out.println("______________________________________");

    }
}